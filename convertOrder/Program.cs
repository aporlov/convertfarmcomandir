﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using FirebirdSql.Data.FirebirdClient;

namespace ConvertOrder
{
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon =
//                @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 0
//ID заказа    : [ZakazCode]
//Клиентский ID: [ZakazCode]
//Дата заказа  : [DataOrder] 
//Позиций      : [Rows]
//Версия EXE   : ZCONVERT
//Версия CFG   : ZCONVERT
//Статус CFG   : ZCONVERT
//Прайс-лист   : [DataOrder] 
//Комментарий  : [Comment]
//";
           
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                #region Проверка параметров
                if (args.Length < 2)
                {
                    System.Console.WriteLine("Convert <файл заявки> <файл шаблона заявки>");
                    return 1;
                }

                if (!System.IO.File.Exists(args[0]) || !System.IO.File.Exists(args[1]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Convert <файл заявки> <файл шаблона заявки>");
                    return 1;
                }       
                #endregion
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + args[0]);
                //Читаем файл заявки
                List<Row> rows = new List<Row>();
                Order order = new Order();
                    IEnumerable<string> LinesCod = File.ReadAllLines(args[0], encoding);
                    bool Flags = false;
                    Cod cod = new Cod() { CodeClient = "", CodeDostavki = "" };
                    string FarmCM_cod="";
                    StringBuilder comment = new StringBuilder();
                    foreach (string item in LinesCod)
                    {

                        if (item.Contains("[ORDER]")) Flags = true;

                        if (!Flags)
                        {
                              string[] buffer = item.Split('=');
                                if (buffer.Length>1)
                                {
                                    switch (buffer[0].ToString().Trim())
                                    {
                                       case "KodOrder": order.CodeZakaz = buffer[1].Trim(); 
                                                    comment.Append("ID=").Append(buffer[1].Trim()).Append("; ");
                                                    break;
                                       case "KodClient": FarmCM_cod = buffer[1].Trim();
                                                    break;
                                       case "KodFil": FarmCM_cod = FarmCM_cod +"_" + buffer[1].Trim();
                                                       cod = GetCodeFromASU(FarmCM_cod);
                                                       order.CodeClient = cod.CodeClient;
                                                       order.CodeDostavki = cod.CodeDostavki;
                                                    break;
                                       case "NameFil": comment.Append(buffer[1].Trim()).Append(", ");   
                                                    break;
                                       case "Date": order.DataOrder= buffer[1].Trim(); 
                                                   break;
                                       case "Address": comment.Append(buffer[1].Trim()).Append("; ");
                                                   break;
                                       case "Sum": comment.Append("Сумма: ").Append(buffer[1].Trim()).Append(";");
                                                   break;
                                       case  "Lines": comment.Append("Позиций: ").Append(buffer[1].Trim());
                                                    break;

                                    }
                                   
                                }
                            
                          
                        }
                        else
                        { 
                            if (item.Contains("[ORDER]") || item.Contains("[END]"))
                            {

                            }
                            else
                            {
                               
                                string[] buffer = item.Split('\t');
                                rows.Add(new Row(){ Code = buffer[0], Count = buffer[4]});
                            }
                        }

                    }
                    order.Comment = comment.ToString();
                    order.Rows = rows.Count.ToString();
                    order.rows = rows;
                   //формируем заявку СИА
                    IEnumerable<string> shablon = File.ReadLines(args[1], encoding);
                    StringBuilder ordersia = new StringBuilder();
                    ordersia.Append(string.Join<string>("\r\n", shablon).Replace("[CodeZakaz]", order.CodeZakaz)
                                                                        .Replace("[CodeClient]", order.CodeClient)
                                                                        .Replace("[CodeDostavki]", order.CodeDostavki)
                                                                        .Replace("[AddressDostavki]", order.AddressDostavki)
                                                                        .Replace("[DataOrder]", order.DataOrder)
                                                                        .Replace("[Rows]", order.rows.Count.ToString())
                                                                        .Replace("[Code]", order.rows[0].Code)
                                                                        .Replace("[Count]", order.rows[0].Count)
                                                                        .Replace("[Comment]",order.Comment)).AppendLine();
                    foreach (var item in order.rows)
                    {
                        ordersia.Append(new string(' ', 50 - item.Code.Length)).Append(item.Code).Append(new string(' ', 10 - item.Count.Length)).Append(item.Count).AppendLine();
                    }
                    //пишем в файл 
                    File.WriteAllText(args[0] + ".txt", ordersia.ToString(), encoding);

            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }

            return 0;
        }

        private static Cod GetCodeFromASU(string addresscode)
        {
            Cod cod = new Cod();
            using (FbConnection RefConnection = new FbConnection("User ID=TWUSER;Password=54321; Database=./db/Siafil.gdb; DataSource=asusrv; Charset=NONE; providerName=\"System.Data.SqlClient\""))
            {
                try
                {

                    RefConnection.Open();
                    FbCommand readCommand = new FbCommand(String.Format("select  nid,nidlib from adrdost a join nab n on a.adrdostid = n.nid where a.adrdostcode='{0}'", addresscode), RefConnection);
                    FbDataReader myreader = readCommand.ExecuteReader();
                    int i = 0;
                    while (myreader.Read())
                    {
                        i++;
                        if (i == 2) throw new Exception("Найдено несколько клиентов с этим кодом");
                        cod.CodeDostavki = myreader.GetString(0);
                        cod.CodeClient = myreader.GetString(1);
                    }
                    RefConnection.Close();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            return cod;
        }
        public class Order
        {
            public string CodeClient { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string Comment { get; set; }
            public List<Row> rows { get; set; }
            public string CodeZakaz { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
        }
        public class Cod
        {
            public string CodeDostavki { get; set; }
            public string CodeClient { get; set; }
        }

    }
}
